﻿using Acr.UserDialogs;
using Covid19.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Covid19.ViewModel
{
   public class AboutViewModel:BaseViewModel
    {
        public AboutViewModel() {
            try
            {
                AboutTeamModel = new AboutTeamModel() {
                CardsTopImage = App.BaseImageUrl + "Mask.png",
                AboutteamData =new List<Aboutteam>() {     
                   
                    new Aboutteam()  { 
                        linkSocial = new List<Sociallinktask>() { 
                            new Sociallinktask() { 
                                Csharpcorner = "https://www.c-sharpcorner.com/members/xamarin-skills",
                                YtLink = "https://www.youtube.com/channel/UCmcWO6GbETogq_ULHf1PX2A", 
                                LinkedLink = "linkedin://profile/company/sumit-sisodia-5a5868a4/", 
                                BloggerLink= "https://xamarinskills.blogspot.com/" ,
                                InstaLink = "instagram://user?username=sumit_sisodia77", 
                            } 
                        }, 
                        City = "Indore", 
                        FirstName = "Sumit", 
                        LastName = "Sisodia",  
                        Designation = "Xamarin Developer",
                        Description="I have 3+ year of experience in hybrid mobile development using xamarin.forms with C#. He is enthusiastic and take an interest in developing new things. He above in watching movies, He a good blogger, generally wrote blogs on xamarin forms.",
                        ImageUrl="https://csharpcorner-mindcrackerinc.netdna-ssl.com/UploadFile/AuthorImage/9cd3bf20200522093419.jpg"
                    },
                     new Aboutteam() {
                        linkSocial = new List<Sociallinktask>() {
                            new Sociallinktask() {
                                InstaLink = "instagram://user?username=sagziie",
                                BloggerLink= "https://www.behance.net/sagzie",
                                YtLink = "",  
                                Csharpcorner = "",
                                LinkedLink = "" ,
                               }
                        },
                        City = "Ahemdabad",
                        FirstName = "Sagar",
                        LastName = "Panchal",
                        Designation = "UI/XD Designer" ,
                        Description = "I have 5+ year of experience in UI/UX development using xamarin.forms with C#. He is enthusiastic and take an interest in developing new things. He above in watching movies.",
                        ImageUrl="https://mir-s3-cdn-cf.behance.net/user/115/b6478f18287823.5676855c9a3df.jpg"
                    }
                 }
            };

                MobIfo = new DeviceInformation();
            }
            catch (Exception ex)
            { 
            }
             
        }

        private string about= "During the COVID pandemic situation all over the world, we get an idea to develop an app which has tracking record of the cases by counties and also show safety measures aims.";
        public string About
        {
            get { return about; }
            set 
            { 
                about = value; 
                RaisePropertyChanged(() =>About); 
            }
        }


        private DeviceInformation mobinfo ;
        public DeviceInformation MobIfo
        {
            get { return mobinfo; }
            set { mobinfo = value; RaisePropertyChanged(() => MobIfo); }
        }

        private AboutTeamModel aboutteammodel;

        public AboutTeamModel AboutTeamModel
        {
            get { return aboutteammodel; }
            set 
            {
                aboutteammodel = value;
                RaisePropertyChanged(() => AboutTeamModel); 
            }
        }
         
        public ICommand SocialCommand
        {
            get
            {
                return new Command<string>((x) => SocialCommandExecution(x));
            }
        }
        public void SocialCommandExecution(string x)
        {
            if (string.IsNullOrEmpty(x))
                UserDialogs.Instance.Alert("We have no url", "Alert", "OK");
            else
                try
                { 
                    Device.OpenUri(new Uri(x)); 
                }
                catch (Exception ex)
                {
                     
                }
            
            //handle parameter x to say "Hello " + x
        }
         
    } 
}

public  class DeviceInformation
{
    public string DeviceModel { get { return Xamarin.Essentials.DeviceInfo.Model; } }
    public string DeviceManufacturer { get { return Xamarin.Essentials.DeviceInfo.Manufacturer; } }
    
    public string DeviceVersionString { get { return Xamarin.Essentials.DeviceInfo.VersionString; } }
    public string DevicePlatform { get { return Xamarin.Essentials.DeviceInfo.Platform.ToString(); } }
    public string DeviceType { get { return Xamarin.Essentials.DeviceInfo.Idiom.ToString(); } }
}