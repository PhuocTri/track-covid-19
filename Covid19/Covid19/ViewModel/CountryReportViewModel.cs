﻿using Acr.UserDialogs;
using Covid19.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Covid19.ViewModel
{
    public class CountryReportViewModel:BaseViewModel
    {
        public CountryReportViewModel()
        {
            GetCountryDataCommand.Execute(null);
        }


        #region Commands
        public Command GetCountryDataCommand => new Command(async () => await GetCountryDataCommandExecution());

        private ObservableCollection<CountryListData> countryList;

        public ObservableCollection<CountryListData> CountryLists
        {
            get { return countryList; }
            set { countryList = value; RaisePropertyChanged(() => CountryLists); }
        }

        private ObservableCollection<CountryModel> allcountry;

        public ObservableCollection<CountryModel> AllCountry
        {
            get { return allcountry; }
            set { allcountry = value; RaisePropertyChanged(() => AllCountry); }
        }

        #endregion

        #region CommandExecution
        private async Task GetCountryDataCommandExecution()
        {
            UserDialogs.Instance.ShowLoading();
            
                // Read RecipeData.json from this Assembly's DataModel folder
                var assembly = GetType().Assembly;
                var resourceName = assembly.GetManifestResourceNames().FirstOrDefault(x => x.EndsWith("Covid19.Resources.countryflag.json", System.StringComparison.OrdinalIgnoreCase));
                if (string.IsNullOrEmpty(resourceName))
                {
                    Debugger.Break();
                 //   return new List<RecipeGroup>();
                }

                // Parse the JSON and generate a collection of RecipeGroup objects
                using (var stream = assembly.GetManifestResourceStream(resourceName))
                using (var reader = new StreamReader(stream))
                {
                    string json = await reader.ReadToEndAsync();
                var AllCountryFlags = JsonConvert.DeserializeObject<List<CountryFlag>>(json);
             
                  //  return result.Groups;
                }
            
        }
        #endregion
    }
}
