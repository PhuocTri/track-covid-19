﻿using Acr.UserDialogs;
using Covid19.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Covid19.ViewModel
{
   public class HomeViewModel:BaseViewModel
    {
        public HomeViewModel() 
        {
            //HomeCommand.Execute(null);
        }
        #region Properties 
         
        
        private CountryModel country;
        public CountryModel Country
        {
            get { return country; }
            set {country = value; RaisePropertyChanged(() => Country); }
        }


        #endregion

        #region Commands 
        public Command WhatsAppCommand => new Command(() => Device.OpenUri(new Uri("https://api.whatsapp.com/send?phone=919013353535")));
        public Command TelegramCommand => new Command(() => Device.OpenUri(new Uri("tg://resolve?domain=MyGovCoronaNewsdesk")));
        public Command InstagramCommand => new Command(() => Device.OpenUri(new Uri("instagram://user?username=mygovindia")));
        public Command FBCommand => new Command(() =>  Device.OpenUri(new Uri("fb://group/224193992332793")));
        public Command MessengerCommand => new Command(() => Device.OpenUri(new Uri("http://m.me/MyGovIndia")));

        //------------------------------------------- 

        public Command PinterestCommand => new Command(() => Device.OpenUri(new Uri("pinterest://www.pinterest.com/mygovindia")));
        public Command YouTubeCommand => new Command(() => Device.OpenUri(new Uri("http://www.youtube.com/mygovindia")));
        public Command LinkedinCommand => new Command(() => Device.OpenUri(new Uri("linkedin://profile/company/mygov-india/")));
        public Command TwitterCommand => new Command(() => Device.OpenUri(new Uri("twitter://user?screen_name=mygovindia")));

        #endregion

        #region CommandExecution

        #endregion

    }
} 