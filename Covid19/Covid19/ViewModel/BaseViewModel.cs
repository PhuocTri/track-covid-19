﻿using Covid19.Managers.SettingsManager;
using Covid19.Managers.UserManager;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Covid19.ViewModel
{
    public class BaseViewModel : ViewModelBase
    {
        private bool isbusy;

        public bool IsBusy
        {
            get { return isbusy; }
            set { isbusy = value; RaisePropertyChanged(() => IsBusy); }
        }

        protected readonly IUserManager userManager;
        internal readonly ISettingsManager settingsManager;
        public BaseViewModel()
        {
            userManager = SimpleIoc.Default.GetInstance<IUserManager>();
            settingsManager = SimpleIoc.Default.GetInstance<ISettingsManager>();
        }
    }
}
