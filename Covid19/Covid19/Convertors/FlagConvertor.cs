﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Covid19.Convertors
{
    public class FlagConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string flagurl = default(string);
            var country = (string)value;
            try
            { 
                if (country != null)
                {
                    if (country.ToLower() == "world")
                        flagurl = "https://image.flaticon.com/icons/png/128/814/814513.png";
                    else
                        flagurl = App.AppSetup.GloalViewModel.CountryFlags.Where(e => e.country.ToLower()==country.ToLower()).Select(e=>e.flag).SingleOrDefault();
                } 
            }
            catch (Exception ex)
            {
            }
            return flagurl==null?  "https://image.flaticon.com/icons/png/128/825/825205.png": flagurl;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
